import React, { Component } from 'react'

const Input = props => {
    return (
        <input type="text" value={props.value} 
            onChange={event => {
                props.changeValue(event.target.value)
            }} 
        />
    )
}

export default Input